﻿using UnityEngine;
using System.Collections;

public class Gavel : MonoBehaviour {

    Animator anim;
    //bool gavelUp = true;
    public int gavelDamage = 10;
	AudioSource smashSound;
	public AudioClip sound;
    public float wallOffset;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        anim.SetBool("gavelUp", true);
        GetComponent<CircleCollider2D>().enabled = false;
		smashSound = GetComponent<AudioSource>();

	}
	
	// Update is called once per frame
	void Update () {
	}

    public void gavelDown()
    {
		if (!smashSound.isPlaying)
		{
			smashSound.PlayOneShot(sound);
		}

		anim.SetBool("gavelUp", false);
        //GetComponent<CircleCollider2D>().enabled = true;
        Invoke("colliderOn", 0.2f);
	}

    public void gavelDownOnWall()
    {
        if (!smashSound.isPlaying)
        {
            smashSound.PlayOneShot(sound);
        }
        transform.position = new Vector2(transform.position.x, transform.position.y + wallOffset);
        anim.SetBool("gavelUp", false);
        //GetComponent<CircleCollider2D>().enabled = true;
        Invoke("colliderOn", 0.2f);
        Invoke("resetPosition", 0.5f);
    }

    void resetPosition()
    {
        transform.position = new Vector2(transform.position.x, transform.position.y - wallOffset);
    }

    void colliderOn()
    {
        GetComponent<CircleCollider2D>().enabled = true;
    }

    public void gavelUp()
    {
        anim.SetBool("gavelUp", true);
        GetComponent<CircleCollider2D>().enabled = false;
    }

    void OnTriggerEnter2D(Collider2D col){
        if(col.tag == "Player")
        {
            col.GetComponent<PlayerController>().takeDamage(gavelDamage);
        }
        else if(col.tag == "BrickWall")
        {
            // One shot the wall
            col.GetComponent<WallHP>().takeDamage(col.GetComponent<WallHP>().getHealth());
        }
    }
}
