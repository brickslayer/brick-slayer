﻿using UnityEngine;
using System.Collections;

public class EnemySpawn : MonoBehaviour {

    public GameObject enemy;
    public float spawnRate = 5f;
    public float nextEnemy = 0f;
    public int enemiesSpawned;
    public int maxEnemies;
    bool playerInRange= false;
    string enemyName = "Spawned Enemies";
    Transform enemyHolder;

	// Use this for initialization
	void Start () {
        enemiesSpawned = 0;
        enemyHolder = new GameObject(enemyName).transform;
        enemyHolder.parent = transform;
	}
	
	// Update is called once per frame
	void Update () {
        // I want it to turn on whent he player steps in range
        // I don't want it to turn off once it's triggered
        if (playerInRange)
        {
            if (Time.time > nextEnemy && enemiesSpawned < maxEnemies)
            {
                nextEnemy = Time.time + spawnRate;
				GameObject clone = (GameObject) Instantiate(enemy, transform.position, transform.rotation) as GameObject;
				clone.GetComponent<decision_tree> ().SetSpawn(this.transform.position);
                clone.transform.parent = enemyHolder;
                enemiesSpawned ++;
            }
        }
	}
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Player")
        {
            playerInRange = true;
        }
    }

    public void setMaxEnemies(int e)
    {
        Debug.Log("setMaxEnemies");
        maxEnemies = e;
    }
}
