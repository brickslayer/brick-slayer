﻿using UnityEngine;
using System.Collections;

public class BrickMover : MonoBehaviour {

    public float speed = 10.0f;
    public int brickDamage = 5;
    public float bounceBackForce = -0.1f;
    bool live = true;
    bool alive;
	AudioSource hitSound;
    

	// Use this for initialization
	void Start () {
        FindObjectOfType<PGMapGenerator>().OnDestroyMap += Despawn;
        alive = true;
		hitSound = GetComponent<AudioSource>();
	}
	
    void Update()
    {
        if(!alive)
        {
            Destroy(this.gameObject);
        }
    }

    void Despawn()
    {
        alive = false;
    }

    public void throwBrick(Vector2 vel, Vector3 rot)
    {
        // WIP
        // Trying to make throwing into one function, not 4.
        GetComponent<Rigidbody2D>().velocity = vel * speed;
        GetComponent<Rigidbody2D>().transform.Rotate(rot);
    }

	public void case0(){
		GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed);
	}

	public void case1(){
		GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);
		GetComponent<Rigidbody2D>().transform.Rotate(new Vector3(0, 0, 90));
	}

	public void case2(){
		GetComponent<Rigidbody2D>().velocity = new Vector2(0, -speed);
		GetComponent<Rigidbody2D>().transform.Rotate(new Vector3(0, 0, 180));
	}

	public void case3(){
		GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, 0);
		GetComponent<Rigidbody2D>().transform.Rotate(new Vector3(0, 0, -90));
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if ((other.tag == "enemy" || other.tag == "Wall" || other.tag== "Player") && live)
        {
			hitSound.Play();
            if (other.tag == "enemy")
            {
				if(other.GetComponent<EnemyMove>()){
                other.GetComponent<EnemyMove>().takeDamage(brickDamage);
				}
				else if(other.GetComponent<BossController>())
				{
					other.GetComponent<BossController>().takeDamge(brickDamage);
				}
			}
			else if(other.tag == "Player")
			{
					other.GetComponent<PlayerController>().takeDamage(brickDamage);
			}
            live = false;
            spinoff();

        } else if (other.tag == "Player" && !live)
        {
            //pickup
            other.GetComponent<PlayerController>().pickup(gameObject);
        }
    }

    void spinoff()
    {
        // Make the brick bounce off targets and become a pickup
        // TODO Need to make the bricks move at the angle they are
        brickDamage = 0;
        int direction = Random.Range(-45, 45);
        GetComponent<Rigidbody2D>().transform.Rotate(0, 0, direction);
        GetComponent<Rigidbody2D>().velocity = 
            new Vector2(GetComponent<Rigidbody2D>().velocity.x * bounceBackForce, 
                        GetComponent<Rigidbody2D>().velocity.y * bounceBackForce);
        Invoke("stop", 0.4f);
        
    }
    void stop()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }
}
