﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    // Character traits
	public float maxSpeed = 5.0f;
	int facing = 2;		                    	// 0 is up, 1 is right, 2 is down, 3 is left
    public float turnAntiDelayOffset = 0.5f;    // This is to make changing directional animations smoother
	public int playerHealth;
    public int playerMaxHealth = 100;
    public int lives = 3;
    public int bricks;
	[HideInInspector] public bool alive = true;
    float hMove, vMove;

    bool invulnerable = false;

    // Brick traits
    public GameObject thrownBrick;
    public Transform throwSpawn;
    public float fireRate = 0.5f;
    private float nextFire = 0.0f;

    // Melee traits
    public float meleeRate = 0.25f;
    private float nextMelee = 0f;

	//co-op controlls
	public string Horizontal= "Horizontal_P1";
	public string Vertical= "Vertical_P1";
	public string Fire1= "Fire1_P1";
	public string Fire2= "Fire2_P1";
	public string Fire3= "Fire3_P1";
	public string HoldFacing= "HoldFacing_P1";


	Animator anim;

	//Melee 
	public GameObject Sledge;
	public GameObject meleeup;
	public GameObject meleeleft;
	public GameObject meleedown;
	public GameObject meleeright;
	public int damage;
	bool IsSwinging= false;
	public bool HasSledge=false;
	float rotationleft=360;
	public float rotationspeed=50;
    public float sledgeMultiplier;

	//Lay Bricks
	public int costofWall;
	public GameObject Wall180;

	AudioSource[] sounds;
	AudioSource footSteps;
	AudioSource pickupSound;
	AudioSource Hammer;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
        playerHealth = playerMaxHealth;
        //StartCoroutine(flash());
		sounds = GetComponents<AudioSource>();
		if (sounds.Length > 0)
		{
			footSteps = sounds[0];
		}
		if (sounds.Length > 1)
		{
			pickupSound = sounds[1];
		}
		if(sounds.Length >2 )
		{
			Hammer =sounds[2];
		}
	}
	
	// Update is called once per frame
	void Update () {
        if (alive)
        {
            // Get button presses here
            if (Input.GetButton(Fire1) && Time.time > nextFire)
            {
                if (bricks >= 1)
                {
                    nextFire = Time.time + fireRate;
                    GameObject Brick = null;
                    /*
				if(facing==0)	
					Brick=(GameObject)Instantiate(thrownBrick,(throwSpawn.position + new Vector3(0,.5f,0)),throwSpawn.rotation);
				else if(facing==1)	
					Brick=(GameObject)Instantiate(thrownBrick,(throwSpawn.position + new Vector3(.5f,0,0)),throwSpawn.rotation);
				else if(facing==2)
					Brick=(GameObject)Instantiate(thrownBrick,(throwSpawn.position + new Vector3(0,-.5f,0)),throwSpawn.rotation);
				else if (facing==3)
					Brick=(GameObject)Instantiate(thrownBrick,(throwSpawn.position + new Vector3(-.5f,0,0)),throwSpawn.rotation);
				if(facing==0)
					Brick.GetComponent<BrickMover>().case0();
				else if(facing==1)
					Brick.GetComponent<BrickMover>().case1();
				else if(facing==2)
					Brick.GetComponent<BrickMover>().case2();
				else if(facing==3)
					Brick.GetComponent<BrickMover>().case3();
                */

                    // Switch statement to add readability.
                    // Also, grouping both statements together for readability
                    switch (facing)
                    {
                        case 0:
                            Brick = (GameObject)Instantiate(thrownBrick, (throwSpawn.position + new Vector3(0, .5f, 0)), throwSpawn.rotation);
                            Brick.GetComponent<BrickMover>().case0();
                            break;
                        case 1:
                            Brick = (GameObject)Instantiate(thrownBrick, (throwSpawn.position + new Vector3(.5f, 0, 0)), throwSpawn.rotation);
                            Brick.GetComponent<BrickMover>().case1();
                            break;
                        case 2:
                            Brick = (GameObject)Instantiate(thrownBrick, (throwSpawn.position + new Vector3(0, -.5f, 0)), throwSpawn.rotation);
                            Brick.GetComponent<BrickMover>().case2();
                            break;
                        case 3:
                            Brick = (GameObject)Instantiate(thrownBrick, (throwSpawn.position + new Vector3(-.5f, 0, 0)), throwSpawn.rotation);
                            Brick.GetComponent<BrickMover>().case3();
                            break;
                    }
                    // Doesn't really work. Won't throw if not moving
                    // Lets you throw at an angle
                    // hMove and vMove are only -1, 0, or 1 b/c of GetAxisRaw
                    //Brick.GetComponent<BrickMover>().throwBrick(new Vector2(hMove, vMove),
                    //                                           new Vector3(0, 0, 90 * facing));
                    bricks--;
                }

            }
			
			if(IsSwinging){
				float rotation=rotationspeed*Time.deltaTime;
				if (rotationleft > rotation)
				{
					rotationleft-=rotation;
				}
				else
				{
					rotation=rotationleft;
					rotationleft=0;
					nextMelee = Time.time + meleeRate/2;
					Invoke("Sledgeoff", 0.01f);
				}
				Sledge.transform.Rotate(0,0,rotation);
				
			}
            //melee attack
            if (Input.GetButton(Fire2) && Time.time > nextMelee)
            {
                if(HasSledge){
					nextMelee = Time.time + meleeRate +100;
					
					// damage=damage/4 +damage;
                	Sledge.SetActive(true);
                	IsSwinging=true;
                	Hammer.Play();
            		rotationleft=360;
                }
                else
                {
					nextMelee = Time.time + meleeRate;
					if (facing == 0)
                	{
                    	meleeup.GetComponent<Collider2D>().enabled = true;
                    	anim.SetTrigger("Attack0");
                	} else if (facing == 1)
                	{
                    	meleeright.GetComponent<Collider2D>().enabled = true;
                    	anim.SetTrigger("Attack1");
                	} else if (facing == 2)
                	{
                    	meleedown.GetComponent<Collider2D>().enabled = true;
                    	anim.SetTrigger("Attack2");
                	} else if (facing == 3)
                	{
               		     meleeleft.GetComponent<Collider2D>().enabled = true;
               		     anim.SetTrigger("Attack3");
               		}
               		Invoke("meleeOff", 0.1f);
                }
            }

            //Lay Wall
            if (Input.GetButtonDown(Fire3) && (bricks >= costofWall))
            {
                GameObject Wall = null;
                /*
			if(facing==0)	
				Wall=(GameObject)Instantiate(Wall180,(throwSpawn.position + new Vector3(0,.3f,0)),throwSpawn.rotation);
			else if(facing==1)	
				Wall=(GameObject)Instantiate(Wall180,(throwSpawn.position + new Vector3(.3f,0,0)),throwSpawn.rotation);
			else if(facing==2)
				Wall=(GameObject)Instantiate(Wall180,(throwSpawn.position + new Vector3(0,-.3f,0)),throwSpawn.rotation);
			else if (facing==3)
				Wall=(GameObject)Instantiate(Wall180,(throwSpawn.position + new Vector3(-.3f,0,0)),throwSpawn.rotation);
			if(facing==1)
				Wall.GetComponent<WallHP>().case1();
			else if(facing==2)
				Wall.GetComponent<WallHP>().case2();
			else if(facing==3)
				Wall.GetComponent<WallHP>().case3();
            */

                // Switch statement to add readability.
                // Also, grouping both statements together for readability
                switch (facing)
                {
                    case 0:
                        Wall = (GameObject)Instantiate(Wall180, (throwSpawn.position + new Vector3(0, .3f, 0)), throwSpawn.rotation);
                        break;
                    case 1:
                        Wall = (GameObject)Instantiate(Wall180, (throwSpawn.position + new Vector3(.3f, 0, 0)), throwSpawn.rotation);
                        Wall.GetComponent<WallHP>().case1();
                        break;
                    case 2:
                        Wall = (GameObject)Instantiate(Wall180, (throwSpawn.position + new Vector3(0, -.3f, 0)), throwSpawn.rotation);
                        Wall.GetComponent<WallHP>().case2();
                        break;
                    case 3:
                        Wall = (GameObject)Instantiate(Wall180, (throwSpawn.position + new Vector3(-.3f, 0, 0)), throwSpawn.rotation);
                        Wall.GetComponent<WallHP>().case3();
                        break;
                }

                bricks -= costofWall;
            }

        }
	}

	void FixedUpdate() {
		if (playerHealth == 0)
		{
            if(lives > 0)
            {
                lives --;

                toggleInvulnerable();
                InvokeRepeating("doflash", 0.1f, 0.3f);
                Invoke("CancelInvoke", 2.3f);
                Invoke("toggleInvulnerable", 2.2f);
                //beginFlash = true;
                playerHealth = 100;
            }
            else
                Death();    // Use this function to handle death
		}

        if (alive)
        {
            hMove = Input.GetAxisRaw(Horizontal);          // Raw makes it less floaty.
            anim.SetFloat("hMove", hMove);
            vMove = Input.GetAxisRaw(Vertical);
            anim.SetFloat("vMove", vMove);

			if (Mathf.Abs(hMove) > 0 || Mathf.Abs(vMove) > 0)
			{
				footSteps.mute = false;
			}
			else
			{
				footSteps.mute = true;
			}

            // Makes you move same speed in any direction. Otherwise, you move faster on an angle
            GetComponent<Rigidbody2D>().transform.Translate(Vector3.ClampMagnitude(new Vector3(
                hMove * maxSpeed * Time.deltaTime, vMove * maxSpeed * Time.deltaTime), maxSpeed * Time.deltaTime));

            // If you hold down "HoldFacing" (default: shift), don't cahnge direction
            if (!Input.GetButton(HoldFacing))
            {
                // Faces the direction with the greatest magnitude
                // Stays on current direction until current direction slows
                if (hMove > 0 && hMove + turnAntiDelayOffset > Mathf.Abs(vMove))
                {
                    facing = 1;
                } else if (hMove < 0 && Mathf.Abs(hMove) + turnAntiDelayOffset > Mathf.Abs(vMove))
                {
                    facing = 3;
                } else if (vMove > 0 && vMove + turnAntiDelayOffset > Mathf.Abs(hMove))
                {
                    facing = 0;
                } else if (vMove < 0 && Mathf.Abs(vMove) + turnAntiDelayOffset > Mathf.Abs(hMove))
                {
                    facing = 2;
                }
            }
            anim.SetInteger("Direction", facing);
        }
	}
    public int getFacing()
    {
        return facing;
    }

	public void takeDamage(int dmg)
	{
        if (!invulnerable)
        {
            if (dmg > playerHealth)
            {
                playerHealth = 0;
            } else
            {
                playerHealth -= dmg;
            }
        }
	}

    void Death()
    {
        anim.SetTrigger("Death");
        alive = false;
        // You are dead. Handle what needs to happen here
        // Disable moving and shooting
    }
    
    void Sledgeoff(){
    	Sledge.SetActive(false);
    	IsSwinging=false;
    }
	
	void meleeOff(){
		if(meleeup.GetComponent<Collider2D>().enabled){
			meleeup.GetComponent<Collider2D>().enabled=false;
		}
		else if(meleeright.GetComponent<Collider2D>().enabled){
			meleeright.GetComponent<Collider2D>().enabled=false;
		}
		else if(meleedown.GetComponent<Collider2D>().enabled){
			meleedown.GetComponent<Collider2D>().enabled=false;
		}
		else if(meleeleft.GetComponent<Collider2D>().enabled){
			meleeleft.GetComponent<Collider2D>().enabled=false;
		}
	}

	public int getDamage(){
		return damage;
	}

    // Method of picking up bricks or other powerups
    public void pickup(GameObject pick)
    {
        if (pick.gameObject.tag == "Brick")
        {
            bricks ++;
            Destroy(pick);
        }
		if (pick.gameObject.tag == "HealthPack")
		{
			playerHealth = 100;
			Destroy(pick);
		}
		if (pick.gameObject.tag == "LifePack")
		{
			lives++;
			Destroy(pick);
		}
		if (pick.gameObject.tag == "BrickPack")
		{
			bricks = bricks + 5;
			Destroy(pick);
		}
		if(pick.gameObject.tag == "Hammer")
		{
			HasSledge=true;
			Destroy(pick);
		}
		pickupSound.Play();
    }

    // Initialized the sledge once
    public void enableSledge()
    {
        if(!HasSledge)
        {
            HasSledge=true;
            damage = (int)(damage * sledgeMultiplier);
            meleeRate=0.7f;
        }
    }

    public int getHealth()
    {
        return playerHealth;
    }

    void doflash()
    {
        GetComponent<SpriteRenderer>().enabled = !GetComponent<SpriteRenderer>().enabled;
    }

    void toggleInvulnerable()
    {
        invulnerable = !invulnerable;
    }
}
