﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

    public GameObject entity;
    Animator anim;
    int health;
    int maxHealth;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (entity.gameObject.tag == "Player")
        {
            health = entity.GetComponent<PlayerController>().getHealth();
            anim.SetFloat("health", health/100f);
	
        } 
        else if (entity.gameObject.tag == "enemy")
        {
            if(entity.GetComponent<EnemyMove>())
            {
                health = entity.GetComponent<EnemyMove>().getHealth();
                maxHealth = entity.GetComponent<EnemyMove>().getMaxHealth();
                anim.SetFloat("health", health/(float)maxHealth);
            }
            else if(entity.GetComponent<BossController>())
            {
                health = entity.GetComponent<BossController>().getHealth();
                maxHealth = entity.GetComponent<BossController>().getMaxHealth();
                anim.SetFloat("health", health/(float)maxHealth);
            }

        }
    }
}
