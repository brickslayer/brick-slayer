﻿using UnityEngine;
using System.Collections;

public class changeLevel : MonoBehaviour {


    //public PGMapGenerator map;

	// Use this for initialization
	void Start () {
        //map = GetComponent<PGMapGenerator>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.tag == "Player")
        {

            Debug.Log("Current Level: "+GetComponentInParent<PGMapGenerator>().currentLevel);
            //GetComponentInParent<PGMapGenerator>().randomizeLevel();
            if(GetComponentInParent<PGMapGenerator>().currentLevel > 5)
            {
                // Win screen is 3
                Application.LoadLevel(3);
            }

            /*
            if(GetComponentInParent<PGMapGenerator>().currentLevel % 3 == 0)
            {
                // set the last room to be a boss room
                Debug.Log("Trying to set room "+(GetComponentInParent<PGMapGenerator>().rooms.Length -1)+" to a boss room");
                GetComponentInParent<PGMapGenerator>().rooms[GetComponentInParent<PGMapGenerator>().rooms.Length -1].bossRoom = true;
            }
            */


            GetComponentInParent<PGMapGenerator>().changeSpawn(transform.position);
            GetComponentInParent<PGMapGenerator>().GenerateMap();
        }


        /*
         * Old way with 6 premade scenes
        currentLevel = Application.loadedLevel;
         && currentLevel < 6)
        {
            Application.LoadLevel(currentLevel + 1);
            // next level
        }
        */
    }
}
