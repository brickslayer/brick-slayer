﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIController : MonoBehaviour {

	public GameObject pauseMenu;
	public GameObject gameOver;
	public Text bricks;
	public Text lives;
    public Text floor;

	private bool isPaused;
	
    public PGMapGenerator map;
	public PlayerController player;
	public GameObject aStar;

	// Use this for initialization
	void Start () 
	{
		pauseMenu.SetActive(false);
		gameOver.SetActive(false);
		isPaused = false;

		player = FindObjectOfType(typeof(PlayerController)) as PlayerController;    // This allows the enemies to find the player instead. Should help when enemies are spawned randomly and not set in the scene
        map = FindObjectOfType(typeof(PGMapGenerator)) as PGMapGenerator;

	}
	
	// Update is called once per frame
	void Update () 
	{
		bricks.GetComponent<UnityEngine.UI.Text>().text = "Bricks: " + player.GetComponent<PlayerController>().bricks;
		lives.GetComponent<UnityEngine.UI.Text>().text = "Lives: " + player.GetComponent<PlayerController>().lives;
        floor.GetComponent<UnityEngine.UI.Text>().text = "Floor: " + map.GetComponent<PGMapGenerator>().currentLevel;

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (isPaused)
			{
				unpauseButtonAction();
				isPaused = false;
			}
			else
			{
				pauseAction();
				isPaused = true;
			}
		}

		if (!player.GetComponent<PlayerController>().alive)
		{
			gameOver.SetActive(true);
		}
	}

	public void exitButtonAction()
	{
		player.GetComponent<PlayerController>().enabled = true;
		Application.LoadLevel("MainMenu");
	}

	public void pauseAction()
	{
		pauseMenu.SetActive(true);
		player.GetComponent<PlayerController>().enabled = false;
		Time.timeScale = 0;
		AstarAI[] enemies = FindObjectsOfType(typeof(AstarAI)) as AstarAI[];
		foreach (AstarAI enemy in enemies) {
			enemy.path=null;
		}
	}

	public void unpauseButtonAction()
	{
		Time.timeScale = 1;
		player.GetComponent<PlayerController>().enabled = true;
		pauseMenu.SetActive(false);
		isPaused = false;
	}

	public void exitPauseButtonAction()
	{
		Time.timeScale = 1;
		player.GetComponent<PlayerController>().enabled = true;
		Resources.UnloadUnusedAssets();
		Application.LoadLevel("MainMenu");
	}

	public void restartButtonAction()
	{
		Time.timeScale = 1;
		player.GetComponent<PlayerController>().enabled = true;
		Application.LoadLevel(Application.loadedLevelName);
	}
}
