﻿using UnityEngine;
using System.Collections;

public class BossController : MonoBehaviour {

	public float speed = 0.05f;	// Change this to change speed of enemy
	public int health = 50;
	public int HealthRegen = 10;
	public int Maxhealth;
	public int attackDamage = 10;
	public float attackSpeed = 5.0F;
	private float nextFire = 2.0f;
	float counter;
	float waitingTime;

	private bool waiting;
	public float returntime = .001f;

	public PlayerController player;
    public GameObject newLevelLadder;

	public GameObject throwWeapon;
	bool Eating = false;
	Animator anim;
	private bool alive;
	private bool weaponInHand;

	private Transform throwWeaponTrans;
	private SpriteRenderer throwWeaponSprite;


	private Transform bossTransform;
	private Vector3 playerPosition;
	private Vector3 throwPoint;

	float deltaX;
	float deltaY;

	public float toPlayer;

	private bool returnToBoss;

	AudioSource voice;
	public AudioClip yourefired1;
	public AudioClip yourefired2;
	public AudioClip yourefired3;
	public AudioClip halfHealth;
	public AudioClip deathSound;

	bool halfHealthSpoken;
	bool diedSoundPlayed;
	// Use this for initialization
	void Start () 
	{
		Maxhealth=health;
		anim = GetComponent<Animator>();
		bossTransform = GetComponent<Transform>();

		// find the player
		player = FindObjectOfType(typeof(PlayerController)) as PlayerController;    // This allows the enemies to find the player instead. Should help when enemies are spawned randomly and not set in the scene
		alive = true;
		weaponInHand = true;

		throwWeaponTrans = throwWeapon.GetComponent<Transform>();
		throwWeaponSprite = throwWeapon.GetComponent<SpriteRenderer>();

		// hide the weapon
		//throwWeaponSprite.enabled = false;
		voice = GetComponent<AudioSource>();

		returnToBoss = false;
		waiting = true;
		halfHealthSpoken = false;
		diedSoundPlayed = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		// find player position
		playerPosition = player.transform.position;
		if (weaponInHand)
		{
			throwWeaponTrans.position = bossTransform.position;
		}
		// face player
		if (health < Maxhealth/2 && !halfHealthSpoken && !voice.isPlaying)
		{
			voice.PlayOneShot(halfHealth);
			halfHealthSpoken = true;
		}
	}

	void FixedUpdate()
	{
		if (alive)
		{
			if (weaponInHand)
			{
				if (waiting)
				{
					waitingTime = Time.time + nextFire;
					waiting = false;
				}

				if (waitingTime < Time.time)
				{
					weaponInHand = false;
					returnToBoss = false;
					waiting = true;
					counter = Time.time + returntime;
					throwPoint = playerPosition;
					int soundClip = Random.Range(0,3);

					switch (soundClip)
					{
					case 0:
						if (!voice.isPlaying)
						{
							voice.PlayOneShot(yourefired1);
						}
						break;
					case 1:
						if (!voice.isPlaying)
						{
							voice.PlayOneShot(yourefired2);
						}
						break;
					case 2:
						if (!voice.isPlaying)
						{
							voice.PlayOneShot(yourefired3);
						}
						break;
					default:
						break;
					};
				}
			}

			if (!weaponInHand)
			{
				float step = attackSpeed * Time.deltaTime + 0.2f;

				if(!returnToBoss)
				{
					throwWeaponTrans.position = Vector2.MoveTowards(throwWeaponTrans.position, throwPoint, step);
				}
				if (throwWeaponTrans.position.x.Equals(throwPoint.x) && throwWeaponTrans.position.y.Equals(throwPoint.y))
				{
					returnToBoss = true;
				}

				if(counter < Time.time)
				{
					returnToBoss=true;
				}
				
				if (returnToBoss)
				{
					throwWeaponTrans.position = Vector2.MoveTowards(throwWeaponTrans.position, bossTransform.position, step);
				}
				if (throwWeaponTrans.position.x.Equals(bossTransform.position.x) && throwWeaponTrans.position.y.Equals(bossTransform.position.y)){
					weaponInHand=true;
					returnToBoss=false;
				}
			}

		}
	}		

	
	
	public void wallHit()
	{
		returnToBoss = true;
	}


	public int Getdamage()
	{
		return attackDamage;
	}

	public void OnCollisionEnter2D(Collision2D coll){
		if (health <= (Maxhealth / 2)) {
			if (coll.gameObject.tag == "enemy") {
				Eating = true;
				anim.SetBool("Eating",Eating);
                anim.SetTrigger("EatingTrigger");
				Destroy(coll.gameObject);
				health += HealthRegen;
				Invoke("StopEating", 1f);
			}
		}
		if (coll.gameObject.tag == "Player")
		{
			//damage
		}

	}

	public void takeDamge(int dmg){
		if (weaponInHand) {
			health -= dmg;
		} else {
			health-= (dmg*2);
		}
		if(health<=0){
			if (!diedSoundPlayed)
			{
				voice.PlayOneShot(deathSound);
				diedSoundPlayed = true;
			}
			Death();
		}

	}

	public bool getHairReturn()
	{
		return returnToBoss;
	}

	void StopEating(){
		Eating = false;
		anim.SetBool("Eating", Eating);
	}

	public void Death(){
		//killl him
		alive = false;
        anim.SetTrigger("Death");
        Invoke("cleanup", 4f);

	}
    void cleanup()
    {
        createExit();
        // Make the ladder to the end screen

        Destroy(this.gameObject);
    }

    void createExit()
    {
        GetComponentInParent<PGMapGenerator>().createExit(this.transform);
    }

    public int getHealth()
    {
        return health;
    }

    public int getMaxHealth()
    {
        return Maxhealth;
    }

}
