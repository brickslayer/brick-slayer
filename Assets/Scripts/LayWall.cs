﻿using UnityEngine;
using System.Collections;

public class LayWall : MonoBehaviour {

	public GameObject Wall180;
	public Transform spawn;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	public void Laybricks () {
	}
	public void case0(){
		Instantiate(Wall180,(spawn.position + new Vector3(0,.3f,0)), spawn.rotation);
	}
	public void case1(){
		Instantiate(Wall180,(spawn.position + new Vector3(.3f,0,0)), spawn.rotation);
	}
	public void case2(){
		Instantiate(Wall180,(spawn.position + new Vector3(0,-.3f,0)), spawn.rotation);
	}
	public void case3(){
		Instantiate(Wall180,(spawn.position +  new Vector3(-.3f,0,0)), spawn.rotation);
	}
}
