﻿using UnityEngine;
using System.Collections;

public class HammerPickup : MonoBehaviour {

    bool alive;
    void Start()
    {
        alive = true;
        FindObjectOfType<PGMapGenerator>().OnDestroyMap += Despawn;
        Invoke("stopMoving", 1.0f);
    }
    
    void Update()
    {
        if(!alive)
        {
            Destroy(this.gameObject);
        }
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            //pickup
            other.GetComponent<PlayerController>().HasSledge = true;
            Despawn();
        }
    }
    
    void Despawn()
    {
        alive = false;
    }

    void stopMoving()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }
}
