﻿using UnityEngine;
using System.Collections;

public class Ladder_Attack : MonoBehaviour {

	public GameObject Bad_guy;

    void Start()
    {
        Bad_guy = GetComponentInParent<EnemyMove>().gameObject;
    }

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Player") {
			coll.gameObject.GetComponent<PlayerController> ().takeDamage (Bad_guy.GetComponent<EnemyMove> ().EnemyAttackDamage);
		}
	}
}
