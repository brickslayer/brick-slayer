﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuController : MonoBehaviour {
	
   
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void startButtonPress ()
	{
		Application.LoadLevel(2);
	}

	public void playButtonPress ()
	{
		Application.LoadLevel(6);
	}


	public void multiplayerButtonPress ()
	{
		Application.LoadLevel(6);
	}

	public void controlsButtonPress ()
	{
		Application.LoadLevel(1);
	}

	public void backButtonPress ()
	{
		Application.LoadLevel("MainMenu");
	}

	public void exitButtonPress()
	{
		Application.Quit();
	}



}
