﻿using UnityEngine;
using System.Collections;

public class HairMover : MonoBehaviour {

	public GameObject Tonald;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate ()
	{

	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") 
		{ 
			other.gameObject.GetComponent<Collider2D> ().GetComponent<PlayerController> ().takeDamage(Tonald.GetComponent<BossController>().Getdamage());
		}
		if (!Tonald.GetComponent<BossController>().getHairReturn())
		{
			if (other.gameObject.tag == "Wall") 
			{ 
				Tonald.GetComponent<BossController>().wallHit();
			}
			if (other.gameObject.tag == "BrickWall") 
			{ 
				Tonald.GetComponent<BossController>().wallHit();
				other.gameObject.GetComponent<WallHP>().takeDamage(100);
			}
		}
	}
}
