﻿using UnityEngine;
using System.Collections;

public class Judge : MonoBehaviour {

    //public Transform gavelMaster;
    public Transform gavel;
    public float gavelSpeed;
    public float gavelAttackSpeed;
    public float gavelRange;
    public float returnTime;
    float nextGavelFire;
    Transform playerTarget;
    Quaternion rotation;
    public Transform hammer;
    public bool stuckOnWall = false;

    public float judgeMoveSpeed;

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {

        Vector3 diff = transform.position - GetComponent<EnemyMove>().player.transform.position;
        diff.Normalize();
        
        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        if(Vector2.Distance(transform.position, GetComponent<EnemyMove>().player.transform.position) < gavelRange || stuckOnWall)
        {
            // stop moving
            GetComponent<AstarAI>().walk=false;
            if(nextGavelFire < Time.time)
            {
                nextGavelFire = Time.time + gavelAttackSpeed;
                if(stuckOnWall)
                {
                    gavel.GetComponent<Gavel>().gavelDownOnWall();
                }
                else
                {
                    gavel.GetComponent<Gavel>().gavelDown();
                }
                Invoke("gavelUnSmash", returnTime);
                stuckOnWall = false;
            }
        }
        else
        {
            GetComponent<AstarAI>().walk=true;
            // Move towards the player
            //transform.position = Vector2.MoveTowards(transform.position, GetComponent<EnemyMove>().player.transform.position, judgeMoveSpeed);
        }

	}
    void gavelUnSmash()
    {
        gavel.GetComponent<Gavel>().gavelUp();

    }

    public void createExit()
    {
        Transform hammerClone = Instantiate(hammer, this.transform.position, this.transform.rotation) as Transform;
        // pushes the hammer in a direction
        int xSign = 0;
        int ySign = 0;
        while((xSign == 0) && (ySign == 0))
        {
            xSign = Random.Range(-1, 2);
            ySign = Random.Range(-1, 2);
        }

        // Pushes the spawned hammer away from the exit
        hammerClone.GetComponent<Rigidbody2D>().AddForce(new Vector2(30.0f*xSign, 30.0f*ySign));
        GetComponentInParent<PGMapGenerator>().createExit(this.transform);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.collider.tag == "BrickWall")
        {
            stuckOnWall = true;
            Destroy(col.gameObject, nextGavelFire - Time.time);
            //col.gameObject.GetComponent<WallHP>().takeDamage(col.gameObject.GetComponent<WallHP>().getHealth());
        }
    }

}
