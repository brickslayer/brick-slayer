﻿using UnityEngine;
using System.Collections;

public class Melee_Attack : MonoBehaviour {

	public GameObject player;
    // The extra damage multiplier the player does to walls
    int playerMultiplier = 5;

	void OnTriggerEnter2D(Collider2D coll){

        // Melee attack collides with the circle collider and the box collider
        // Does double damage up close and single damage from too far away

		if (coll.gameObject.tag == "enemy")
        {
            if (coll.gameObject.GetComponent<EnemyMove>())
            {
                //Debug.Log("Collided");
                coll.gameObject.GetComponent<EnemyMove>().takeDamage(player.GetComponent<PlayerController>().getDamage());
            } else
            {
                coll.gameObject.GetComponent<BossController>().takeDamge(player.GetComponent<PlayerController>().getDamage());
            }
        } else if (coll.gameObject.tag == "Player")
        {
            if (coll.gameObject.GetComponent<PlayerController>())
            {
                coll.gameObject.GetComponent<PlayerController>().takeDamage(player.GetComponent<PlayerController>().getDamage());
            }
        } else if (coll.gameObject.tag == "BrickWall")
        {
            if (coll.gameObject.GetComponent<WallHP>())
            {
                coll.gameObject.GetComponent<WallHP>().takeDamage(player.GetComponent<PlayerController>().getDamage() * playerMultiplier);
            }
        }
	}
}
