﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SeedField : MonoBehaviour {

    static int seed;

    public void seedInput()
    {
        seed = System.Convert.ToInt32(GetComponent<InputField>().text);
    }

    public static int getSeed()
    {
        return seed;
    }

    public void randomizeSeed()
    {
        // Limit is arbitrary
        seed = Random.Range(0, 10000);
        GetComponent<InputField>().text = System.Convert.ToString(seed);
    }
}
