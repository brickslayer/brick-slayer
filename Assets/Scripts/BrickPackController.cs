﻿using UnityEngine;
using System.Collections;

public class BrickPackController : MonoBehaviour {

    bool alive;
    void Start()
    {
        alive = true;
        FindObjectOfType<PGMapGenerator>().OnDestroyMap += Despawn;
    }

    void Update()
    {
        if(!alive)
        {
            Destroy(this.gameObject);
        }
    }

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			//pickup
			other.GetComponent<PlayerController>().pickup(gameObject);
		}
	}

    void Despawn()
    {
        alive = false;
    }
}
