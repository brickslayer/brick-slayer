﻿using UnityEngine;
using System.Collections;

public class WallHP : MonoBehaviour {
	public int wallhealth;
    public GameObject bag;
    Animator anim;
    bool alive;
	// Update is called once per frame
	void Start () {
        anim = GetComponent<Animator>();
        FindObjectOfType<PGMapGenerator> ().OnDestroyMap += Despawn;
        alive = true;
	}
	
    void Update() {
        anim.SetFloat("Health", wallhealth);
        if(!alive)
        {
            Destroy(this.gameObject);
        }
    }

	public void case1(){
		GetComponent<BoxCollider2D>().transform.Rotate(new Vector3(0, 0, 90));
	}
	public void case2(){
		GetComponent<BoxCollider2D>().transform.Rotate(new Vector3(0, 0, 180));
	}
	public void case3(){
		GetComponent<BoxCollider2D>().transform.Rotate(new Vector3(0, 0, -90));
	}

	public void takeDamage(int dmg){
		if (dmg >= wallhealth)
		{
			wallhealth = 0;
			removewall();
		} 
		else
		{
			wallhealth -= dmg;
		}
	}

	void removewall(){
		Destroy(this.gameObject);
        Instantiate(bag, transform.position, transform.rotation);
	}

    void Despawn()
    {
        alive = false;
    }

    public int getHealth()
    {
        return wallhealth;
    }
}
