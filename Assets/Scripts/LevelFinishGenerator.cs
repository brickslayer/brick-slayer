﻿using UnityEngine;
using System.Collections;
using System;

public class LevelFinishGenerator : MonoBehaviour 
{
	public float origin = 0;
	public float dimensionX = 0;
	public float dimensionY = 0;
	public float gridSize = 0;
	public GameObject node;

	int[,] map;
	Vector2 playerLocation;
	Vector2 exitLocation;

	// Use this for initialization
	void Start () 
	{
		GeneratePath();
	}
	
	void GeneratePath()
	{
		map = new int[(int)dimensionX, (int)dimensionY];
		// take in start point
		// take in dimensions
		scanMap();

		addExit();
	}

	void addExit()
	{
		float xOffset = 0f;
//		int maxX = 0;
//		int maxY = 0;
		float dist = 0;
		Vector3 location = Vector3.zero;
		for (int i = 0; i < (int)dimensionX; i++)
		{
			float yOffset = 0f;

			for (int j = 0; j < (int)dimensionY; j++)
			{
				if (map[i,j] == 1)
				{
					float xDist = Mathf.Abs(playerLocation.x - i);
					float yDist = Mathf.Abs(playerLocation.y - j);

					float tempDist = Mathf.Sqrt((xDist * xDist) + (yDist * yDist));
					if (tempDist > dist)
					{
						location = new Vector3(xOffset, yOffset, 0);

						dist = tempDist;
						//maxX = i;
						//maxY = j;
						Debug.Log("DISTANCE: " +dist);
					}
				}
				yOffset += 1.28f;

			}
			xOffset += 1.28f;
		}

		Instantiate(node, location, Quaternion.identity);

	}

	void scanMap()
	{
		float xOffset = 0f;

		for (int i = 0; i < (int)dimensionX; i++)
		{
			float yOffset = 0f;
			for (int j = 0; j < (int)dimensionY; j++)
			{
				Vector3 location = new Vector3(xOffset, yOffset, 0);


				Collider2D hitCollider = Physics2D.OverlapCircle(location, 0.1f);
					//Physics.OverlapSphere(location, 0.5f);
				if (hitCollider != null)
				{
					print ("HIT");
					GameObject colCheck = hitCollider.gameObject;
					if (colCheck.tag.Equals("Wall"))
					{
						//print("Wall total: " + wallTotal);
					}
					else if (colCheck.tag.Equals("Player"))
					{
						playerLocation = new Vector2(i, j);
						map[i,j] = 1;
						Debug.Log(playerLocation);
					}
					else
					{
						//map[i,j] = (GameObject) Instantiate(node, location, Quaternion.identity);
						map[i,j] = 1;
					}
				}
				else
				{
					//map[i,j] = (GameObject) Instantiate(node, location, Quaternion.identity);
					map[i,j] = 1;
				}
				yOffset += 1.28f;
			}
			xOffset += 1.28f;
		}
	}
}
