﻿using UnityEngine;
using System.Collections;

public class BoundaryDestroyer : MonoBehaviour {

    // Copy-and-pasted from Unity Documentation
    void OnTriggerExit2D(Collider2D other)
    {
        // Destroy everything that leaves the trigger
        Destroy(other.gameObject);

    }

}
