using UnityEngine;
using System.Collections;

public class decision_tree: MonoBehaviour
{
	delegate void MyDelegate();
	MyDelegate enemyAction;
	float Target_Range= 12f;
	float Melee_range = 2f;
	float Freinds_range= 6;
	int Max_Distance = 8;
	int lowhealth = 5;
	public PlayerController player;
	public EnemyMove enemy;
	public AstarAI Ai;
	bool inMelee_range;
	public float distance;
	public float spawndistance;
	public Vector2 Spawn;
	GameObject[] enemies;
	public bool NotOnWall=true;
	float waittime;
    public Vector2 randomWalkTarget;
	
	void Start()
	{
        enemy = GetComponent<EnemyMove>();
        Ai = GetComponent<AstarAI>();
        waittime = Time.time;
		player = FindObjectOfType(typeof(PlayerController)) as PlayerController; 
		inMelee_range = false;
        Spawn = transform.position;
		enemyAction = TargetInThreat;
		InvokeRepeating ("Wait_decision", 0.25f, 0.25f);
	}
	
	void Update()
	{
		enemies = GameObject.FindGameObjectsWithTag("enemy");
		distance = Vector2.Distance(player.transform.position , transform.position);
		spawndistance = Vector2.Distance(Spawn, transform.position);
		//enemyAction ();
	}
	
	
	/*  Decision Functions  -----------------------------------------------*/
	
	private void TargetInThreat()
	{
		if (distance <= Target_Range ) 
			enemyAction = InRange;
		else
			enemyAction = FarFromSpawn;
	}
	
	private void FarFromSpawn()
	{
		if(spawndistance > Max_Distance)
			enemyAction = MoveToSpawn;
		else
			enemyAction = RandomAction; 
	}
	
	
	private void RandomAction()
	{
			enemyAction = RandomWalk; 
	}
	
	// is enemy within range of a target?
	private void InRange()
	{
		if(distance <= Melee_range)
		{
			inMelee_range=true;
			enemyAction = HpLow;
		}
		else
			enemyAction = Advance; 
	}
	
	
	private void FriendsNear()
	{
		if(CheckCloseToTag(Freinds_range))
			enemyAction = Charge;
		else
			enemyAction = HpLow; 
	}
	
	
	private void HpLow()
	{
		if(enemy.getHealth() <= lowhealth)
			enemyAction = FriendsNear;
		else
			enemyAction = Charge;
	}
	
	
	/*  Action Functions -----------------------------------------------*/


	// walk randomly
	private void RandomWalk()
	{
		enemy.Walk = false;
        enemy.movingToSpawn = false;
        if (waittime<Time.time )            // && NotOnWall
        {
            //print("Does this run? waittime: "+waittime);
			waittime = Time.time + 1;
			enemy.RND_Walk=true;
			enemy.temp = Random.value;

            int randX = Random.Range(-1, 2);
            int randY = Random.Range(-1, 2);
            enemy.moveDirection = 1;
            randomWalkTarget = new Vector2(transform.position.x + randX * 5, transform.position.y +randY*5);
            //Vector2 randCircle = Random.insideUnitCircle;
		}
		enemyAction = TargetInThreat;
	}
	
	// move towards spawn point
	private void MoveToSpawn(){
		Ai.walk = false;
		enemy.Walk=false;
		enemy.RND_Walk = false;
        enemy.movingToSpawn = true;
		enemyAction = TargetInThreat;
	}
	
	// flee from closest target
	private void Flee(){
		enemyAction =TargetInThreat;
	}
	
	// advance towards closest target
	private void Advance(){
		enemy.RND_Walk = false;
		enemy.Walk=false;
		Ai.walk = true;
        enemy.movingToSpawn = false;
		enemyAction = TargetInThreat;
	}
	
	private void Charge(){
		enemy.RND_Walk = false;
		enemy.Walk=true;
		Ai.walk = false;
		enemy.movingToSpawn = false;
		enemyAction = TargetInThreat;
	}
	
	/* extra methods -----------------------------------------------*/
	
	// takes longer to make decisions for demonstration purposes
	
	
	bool CheckCloseToTag(float minimumDistance)
	{
		foreach (GameObject enemy in enemies) {
			if (Vector2.Distance (enemy.transform.position, transform.position) < minimumDistance) 
				return true;
		}
		return false;
	}
	
	
	public void SetSpawn(Vector2 v){
		Spawn = v;
	}
	
	
	private void Wait_decision(){
		enemyAction();
	}
}