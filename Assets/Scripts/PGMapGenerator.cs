﻿using UnityEngine;
using System.Collections;

public class PGMapGenerator : MonoBehaviour {

    public Room[] rooms;
    int roomIndex;
    // total number of rooms. 5-10
    int numberOfRooms = 5;

    //public GameObject path;

    public int seed;
    public Transform floorTile;
    public Transform wallTile;
    public Transform topWallTile;
    public Transform player;
    public Transform enemyType1;
    public Transform enemyType2;
    public Transform exit;
    public Transform[] bosses;
    int bossCounter = 0;

    //public int numOfWalls;
    public int floorCount;

    //public int diggerPositionX;
    //public int diggerPositionY;
    string mapName = "Generated Map";
    string spawnedEntities = "Spawned Entities";

    public float gridSize;
    [Range(0,1)]
    public float percentOpen;
    public Vector2 mapDimensions;
    public int bossRoomSizeX;
    public int bossRoomSizeY;
    public Vector2 spawnPoint;

    Transform[,] map;
    Transform mapHolder;
    public Transform spawnHolder;
    public int currentLevel;

    public event System.Action OnDestroyMap;
    bool destroyRan;

    void Start()
    {
        seed = SeedField.getSeed();
        currentLevel = 0;
        //DestroyMap();
        // Makes the number of rooms, size of rooms, and enemy in them
        randomizeLevel();
        GenerateMap();
    }

    // This destroys the map using destroy
    // This works better when trying to destroy the map to make a new one in game
    public void DestroyMap()
    {
        if(transform.FindChild(mapName))
        {
            Destroy(transform.FindChild(mapName).gameObject);
        }
        if(transform.FindChild (spawnedEntities))
        {
            Destroy(transform.FindChild(spawnedEntities).gameObject);
        }

        if(OnDestroyMap != null)
        {
            OnDestroyMap();
        }
        destroyRan = true;
    }

    public void DestroyMapImmediate()
    {
        // DestroyImmediate is used when changing things in the editor
        // Destroy would delete all the stuff you had just made, after you just made it
        if(transform.FindChild (mapName))
        {
            DestroyImmediate(transform.FindChild(mapName).gameObject);
        }
        if(transform.FindChild (spawnedEntities))
        {
            DestroyImmediate(transform.FindChild(spawnedEntities).gameObject);
        }
        destroyRan = true;
    }

	public void GenerateMap()
    {
        Random.seed = seed;
        currentLevel ++;
        // Call DestroyMap() or DestroyMapImmediate() before you call GenerateMap()
        // immediate from editor script only
        // This if seems to work so you can call just GenerateMap() from other places
        // Such as the changeLevel script and at Start()
        if(!destroyRan)
        {
            DestroyMap();
        }

        destroyRan = false;
        mapHolder = new GameObject(mapName).transform;
        mapHolder.parent = transform;

        spawnHolder = new GameObject(spawnedEntities).transform;
        spawnHolder.parent = transform;



        // Randomizes the number of rooms, size of rooms, and type of spawn in the rooms
        // Including making a boss room on the 3rd level
        randomizeLevel();

        // Makes a map filled with walls
        createMap();

        // Digs out the map, putting down floors
        digger();

        // Renders the map, drawing it to the screen
        // Builds the structure pieces first (walls and floor)
        buildMap(mapHolder);

        // Figures out where to spawn things (enemies, player, etc.)
        spawn();

        // Renders the spawn objects on the screen
        // Builds the non-structure pieces (player and enemies)
        buildMap(spawnHolder);

        // Move the player to spawn
        //Vector2 gridPosition = new Vector2(x + gridSize*x, y + gridSize*y);
        // Correct the spawn location if it's outside the bounds
        if((int)spawnPoint.x <= 0)
        {
            spawnPoint.x = 1;
        }
        else if((int)spawnPoint.x >= (int)mapDimensions.x -1)
        {
            spawnPoint.x = (int)mapDimensions.y -2;
        }
        if((int)spawnPoint.y <= 0)
        {
            spawnPoint.y = 1;
        }
        else if((int)spawnPoint.y >= (int)mapDimensions.y -1)
        {
            spawnPoint.y = (int)mapDimensions.y -2;
        }
        player.transform.position = new Vector2((int)spawnPoint.x + gridSize*(int)spawnPoint.x, (int)spawnPoint.y + gridSize*(int)spawnPoint.y);

        //AstarPathEditor.MenuScan();
        AstarPath.active.Scan();

    }



    // Gets a random number of rooms
    // Sets these rooms to random sizes
    // Puts an enemyType in each of the rooms
    public void randomizeLevel()
    {
        numberOfRooms = Random.Range(5, 10);
        //rooms = new Room[roomIndex];
        //mapDimensions.x = roomIndex * 6;
        //mapDimensions.y = roomIndex * 6;
        for(int i=0; i<numberOfRooms; i++)
        {
            rooms[i].xsize = Random.Range(3, 7);
            rooms[i].ysize = Random.Range(3, 7);
            rooms[i].bossRoom = false;
            if(i%2 == 0)
            {
                rooms[i].spawnType = enemyType1;
            }
            else
            {
                rooms[i].spawnType = enemyType2;
            }
        }
        // every 3 levels, make a boss room
        if(currentLevel % 3 == 0)
        {
            rooms[numberOfRooms - 1].bossRoom = true;
            bossCounter = 0;
            if(currentLevel == 6)
            {
                bossCounter = 1;
            }
        }


    }
    
    void spawn()
    {
        // Set array to null
        for(int x = 0; x < mapDimensions.x; x++)
        {
            for(int y = 0; y < mapDimensions.y; y++)
            {
                map[x, y] = null;
            }
        }
               

        float distanceSqToExit = 0f;
        int furthestRoomIndex = 0;
        bool noBossRoom = true;
        // iterate through the rooms
        // Puts an enemy spawner at the center of each room
        // (i=1): Don't spawn an enemy in the first room, that's where you spawn 
        for(int i=1; i<numberOfRooms; i++)
        {
            // Spawn in the center
            if(rooms[i].bossRoom)
            {
                // Saves the spawn type of the room so it can be changed to a boss
                rooms[i].originalSpawnType = rooms[i].spawnType;
                rooms[i].spawnType = bosses[bossCounter];
                noBossRoom = false;
            }

            map[(rooms[i].xpos -1) + (rooms[i].xsize - rooms[i].xsize/2), (rooms[i].ypos -1) + (rooms[i].ysize - rooms[i].ysize/2)] = rooms[i].spawnType;

            // Finds the distance from spawn to the upper right corner of each room
            float currentSqDistance = Mathf.Pow(((rooms[i].xpos + rooms[i].xsize) - spawnPoint.x), 2) + Mathf.Pow(((rooms[i].ypos + rooms[i].ysize) - spawnPoint.y), 2);

            //Debug.Log("Current: "+currentSqDistance+" Distance: "+distanceSqToExit);
            // If that distance is the biggest so far, save the current room as the furthest
            if(distanceSqToExit < currentSqDistance)
            {
                distanceSqToExit = currentSqDistance;
                furthestRoomIndex = i;
            }
        }

        // Spawn the exit at the furthest away room in the upper right corner
        if(noBossRoom)
        {
            // If there isn't a boss room, have to spawn an exit.
            // Otherwise, the boss dying will spawn the exit
            map[(rooms[furthestRoomIndex].xpos + rooms[furthestRoomIndex].xsize -1), (rooms[furthestRoomIndex].ypos + rooms[furthestRoomIndex].ysize -1)] = exit;
        }


        // clears the array
        for(int x = 0; x < mapDimensions.x; x++)
        {
            for(int y = 0; y < mapDimensions.y; y++)
            {
                if(map[x, y] == wallTile || map[x, y] == floorTile || map[x, y] == topWallTile)
                {
                    map[x, y] = null;
                }
            }
        }

    }

    // Builds whatever transforms are in the map[,] array
    void buildMap(Transform parentTrans)
    {
        for(int x = 0; x < mapDimensions.x; x++)
        {
            for(int y =0; y < mapDimensions.y; y++)
            {
                //mapDimensions.x + floorSize + x, mapDimensions.y + floorSize + y
                Vector2 gridPosition = new Vector2(x + gridSize*x, y + gridSize*y);
                if(map[x, y] != null)
                {
                    Transform newGridSquare = Instantiate(map[x, y], gridPosition, transform.rotation) as Transform;
                   
                    newGridSquare.parent = parentTrans;
                }
            }
        }
    }

    // Given the raw vector3 position of the new spawn point,
    // change that into the spawnPoint map representation
    public void changeSpawn(Vector3 newSpawn)
    {
        spawnPoint = new Vector2(newSpawn.x/(1+gridSize), newSpawn.y/(1+gridSize));
    }

    void buildRoom()
    {

    }

    public void createExit(Transform t)
    {
        Transform e = Instantiate(exit, t.position, t.rotation) as Transform;
        e.parent = spawnHolder;
    }


    void layFloor(int x, int y)
    {
        //print("in layFloor() digger "+ x + ", " + y);
        /*
         * TESTING: 
         * 
         */
        if(x < mapDimensions.x -1 && y < mapDimensions.y -1 && x > 0 && y > 0)
        {
            map[x, y] = floorTile;
            if(map[x, y+1] == wallTile)
            {
                map[x, y+1] = topWallTile;
            }
            floorCount ++;
        }
        else
        {
            Debug.Log("Tried to lay floor out of bounds at: " + x + ", " + y);
        }
    }

    void digger()
    {
        roomIndex = 0;
        // start at spawnPoint and dig from there
        int diggerPositionX = (int)spawnPoint.x + 1;
        int diggerPositionY = (int)spawnPoint.y - rooms[roomIndex].ysize/2;
        if(diggerPositionY < 0)
        {
            diggerPositionY = 0;
        }
        floorCount = 0;

        int emergencyBreak = 1000;
        int loopCounter = 0;


        bool mapUpdate = false;
        bool outOfBounds = false;
        bool topEdge;
        bool rightEdge;
        bool bottomEdge;
        bool leftEdge;
        // Used to populate the map until enough floor tiles were laid
        // Now we go until all the rooms are placed
        // float desiredFloorCount = (mapDimensions.x-2) * (mapDimensions.y-2) * percentOpen;
        float moveProb = 0;
        float turnProb = 0;
        float roomProb = 1;
        float probChange = 0.01f;
        int direction = 0;

        //for(int i = 0; i < desiredFloorCount; i++)
        //while(floorCount < desiredFloorCount)
        while(roomIndex < numberOfRooms)
        {
            //Debug.Log("Start of main for loop: " + diggerPositionX + ", " + diggerPositionY + " Direction: " + direction);
            //Debug.Log("moveProb: "+moveProb+" turnProb: "+turnProb+" roomProb: "+roomProb);

            topEdge = false;
            rightEdge = false;
            bottomEdge = false;
            leftEdge = false;

            // mapUpdate goes first
            if(mapUpdate)
            {
                if(diggerPositionX < mapDimensions.x && diggerPositionY < mapDimensions.y)
                {
                    if(map[diggerPositionX, diggerPositionY] != floorTile)
                    {
                        //map[diggerPositionX, diggerPositionY] = floorTile;
                        //layFloor(diggerPositionX, diggerPositionY);
                        //map[diggerPositionX, diggerPositionY] = floorTile;
                        //Debug.Log("Line 132 before a layFloor(): "+ diggerPositionX + ", " + diggerPositionY);
                        layFloor(diggerPositionX, diggerPositionY);
                    }
                    else
                    {
                        //i--;
                    }
                }
                else
                {
                    Debug.Log("Tried to lay wall out of bounds. mapUpdate is true, so should be move's fault. Position: "+diggerPositionX+", "+diggerPositionY);
                }
            }
            else
            {
                //i--;
            }
       
            if(loopCounter > emergencyBreak)
            {
                print("ran too many times. Efficiency: "+ (float)floorCount/emergencyBreak);
                break;
            }

            float r = Random.Range(0f, 1f);
            if(0 <= r && r < moveProb)
            {
                //print("move");
                /* 
                 * MOVE
                 * 
                 */
                switch(direction)
                {
                    case 0:
                        if(diggerPositionY + 1 < (int)mapDimensions.y - 1)
                        {
                            //Debug.Log("Case 0");
                            mapUpdate = true;
                            diggerPositionY ++;
                            outOfBounds = false;
                        }
                        else
                        {
                            outOfBounds = true;
                            mapUpdate = false;
                        }
                        break;
                    case 1:
                        if(diggerPositionX + 1 < (int)mapDimensions.x - 1)
                        {
                            //Debug.Log("Case 1");
                            mapUpdate = true;
                            diggerPositionX ++;
                            outOfBounds = false;
                        }
                        else
                        {
                            outOfBounds = true;
                            mapUpdate = false;
                        }
                        break;
                    case 2:
                        if(diggerPositionY - 1 > 0)
                        {
                            //Debug.Log("Case 2");
                            mapUpdate = true;
                            diggerPositionY --;
                            outOfBounds = false;
                        }
                        else
                        {
                            outOfBounds = true;
                            mapUpdate = false;
                        }
                        break;
                    case 3:
                        if(diggerPositionX -1 > 0)
                        {
                            //Debug.Log("Case 3");
                            mapUpdate = true;
                            diggerPositionX --;
                            outOfBounds = false;
                        }
                        else
                        {
                            outOfBounds = true;
                            mapUpdate = false;
                        }
                        break;
                    default:
                        break;
                  
                };

                // If there is already a floor tile here, don't try to put another
                // Rerun map making with same probablilities
                if(map[diggerPositionX, diggerPositionY] != floorTile)
                {
                    moveProb -= probChange;
                    turnProb += probChange/2;
                    roomProb += probChange/2;
                }
                else if(map[diggerPositionX, diggerPositionY] == floorTile)
                {
                    // stepped on an existing floor tile
                    // try again
                    mapUpdate = false;
                }
                if(outOfBounds)
                {
                    // facing towards a wall and will keep trying to advance, but be out of bounds every time
                    // want to turn to get away
                    //Debug.Log("OutOfBounds");
                    turnProb = turnProb + moveProb + roomProb;
                    moveProb = 0;
					roomProb = 0;
                    // roomProb = no change
                }


            }
            else if(moveProb <= r && r < (moveProb + turnProb))
            {
                //print("turn");
                /*
                 * TURN
                 * 
                 */
                int turnRand = Random.Range(0,2);
                if(diggerPositionX == 1) // || 
                {
                    // on left edge
                    if(direction == 0 || direction == 2)
                    {
                        // going up or down, change direction to go to the right
                        direction = 1;
                        // Setting this to -1 so it doesn't try to turn
                        // Already set the direction and don't want to turn again
                        // But if this if was false, would still want it to turn
                        // therefore, sometimes want to run the turning, not just changing direction directly
                        turnRand = -1;
                    }
                }
                if(diggerPositionX == (int)mapDimensions.x - 2)
                {
					if(turnRand != -1)	// if it's -1, it has already corrected its turn
					{
	                    // on the right edge
	                    if(direction == 0 || direction == 2)
	                    {
	                        // going up or down, change direction to go to the left
	                        direction = 3;
	                        turnRand = -1;
						}
                    }
                }
                if(diggerPositionY == 1)
                {
					if(turnRand != -1)	// if it's -1, it has already corrected its turn
					{
	                    // on the bottom edge
	                    if(direction == 1 || direction == 3)
	                    {
	                        // going left or right, change direction to go up
	                        direction = 0;
	                        turnRand = -1;
	                    }
					}
                }
                if(diggerPositionY == (int)mapDimensions.y - 2)
                {
					if(turnRand != -1)
					{
	                    // on the top edge
	                    if(direction == 1 || direction == 3)
	                    {
	                        // going left or right, change direction to go down
	                        direction = 2;
	                        turnRand = -1;
	                    }
					}
                }
                if(turnRand == 0)
                {
                    // turn right
                    direction = (direction + 1) % 4;
                }
                else if(turnRand == 1)
                {
                    // turn left
                    direction = (direction + 3) % 4;
                }
                /*
                moveProb += probChange/2;
                turnProb -= probChange;
                roomProb += probChange/2;
                */
                moveProb += turnProb/2;
                roomProb += turnProb/2;
                turnProb = 0f;
            }
            else if((moveProb + turnProb) <= r && r < (moveProb + turnProb + roomProb))
            {
                //print("room from direction: "+direction);
                /*
                 * BUILD A ROOM
                 * 
                 */
                int oldDiggerX = diggerPositionX;
                int oldDiggerY = diggerPositionY;


                // If the room trying to be built is a boss room, make it bigger
                if(rooms[roomIndex].bossRoom)
                {
                    rooms[roomIndex].originalSizeX = rooms[roomIndex].xsize;
                    rooms[roomIndex].originalSizeY = rooms[roomIndex].ysize;
                    rooms[roomIndex].xsize = bossRoomSizeX;
                    rooms[roomIndex].ysize = bossRoomSizeY;
                }

                /*
                 * ASSUME MAP DIMENSIONS ARE LARGER THAN THE
                 * ROOM WE ARE TRYING TO MAKE
                 * 
                 * 
                 */


                switch(direction)
                {
                    // UP
                    case 0:
                        // check if there's room to the left
                        if(diggerPositionX - rooms[roomIndex].xsize/2 <= 0)
                        {
                            leftEdge = true;
                            diggerPositionX = 1 + rooms[roomIndex].xsize/2;
                        }
                        // check if there's room to the right
                        if(diggerPositionX + (rooms[roomIndex].xsize - rooms[roomIndex].xsize/2) >= mapDimensions.x - 1)
                        {
                            rightEdge = true;
                            diggerPositionX = (int)mapDimensions.x - (rooms[roomIndex].xsize - rooms[roomIndex].xsize/2) - 1;
                        }
                        // check if there's room above
                        if(diggerPositionY +rooms[roomIndex].ysize >= mapDimensions.y - 1)
                        {
                            topEdge = true;
                            diggerPositionY = (int)mapDimensions.y - rooms[roomIndex].ysize - 2;
                        }
                        // Get digger position to the local (0,0) of the room
                        diggerPositionX = diggerPositionX - rooms[roomIndex].xsize/2;
                        diggerPositionY ++;
                        break;
                    // RIGHT
                    case 1:
                        // room above
                        if(diggerPositionY + rooms[roomIndex].ysize/2 >= mapDimensions.y - 1)
                        {
                            topEdge = true;
                            diggerPositionY = (int)mapDimensions.y - rooms[roomIndex].ysize/2 - 2;
                        }
                        // room to the right
                        if(diggerPositionX + rooms[roomIndex].xsize >= mapDimensions.x - 1)
                        {
                            rightEdge = true;
                            diggerPositionX = (int)mapDimensions.x - rooms[roomIndex].xsize - 2;
                        }
                        // room below
                        if(diggerPositionY - (rooms[roomIndex].ysize - rooms[roomIndex].ysize/2) < 1)
                        {
                            bottomEdge = true;
                            diggerPositionY = (rooms[roomIndex].ysize - rooms[roomIndex].ysize/2);
                        }
                        // Get digger position to the local (0,0) of the room
                        diggerPositionX ++;
                        diggerPositionY = diggerPositionY - (rooms[roomIndex].ysize - rooms[roomIndex].ysize/2) + 1;
                        break;
                    // DOWN
                    case 2:
                        // check if there's room to the left
                        if(diggerPositionX - rooms[roomIndex].xsize/2 <= 0)
                        {
                            leftEdge = true;
                            diggerPositionX = 1 + rooms[roomIndex].xsize/2;
                        }
                        // check if there's room to the right
                        if(diggerPositionX + (rooms[roomIndex].xsize - rooms[roomIndex].xsize/2) >= mapDimensions.x - 1)
                        {
                            rightEdge = true;
                            diggerPositionX = (int)mapDimensions.x - (rooms[roomIndex].xsize - rooms[roomIndex].xsize/2) - 1;
                        }
                        // check if there's room below
                        if(diggerPositionY - rooms[roomIndex].ysize < 1)
                        {
                            bottomEdge = true;
                            diggerPositionY = 1 + rooms[roomIndex].ysize;
                        }
                        // Get digger position to the local (0,0) of the room
                        diggerPositionX = diggerPositionX - rooms[roomIndex].xsize/2;
                        diggerPositionY = diggerPositionY - rooms[roomIndex].ysize;
                        break;
                    // LEFT
                    case 3:
                        // room above
                        if(diggerPositionY + (rooms[roomIndex].ysize - rooms[roomIndex].ysize/2) >= mapDimensions.y - 1)
                        {
                            topEdge = true;
                            diggerPositionY = (int)mapDimensions.y - (rooms[roomIndex].ysize - rooms[roomIndex].ysize/2) - 1;
                        }
                        // room to the left
                        if(diggerPositionX - rooms[roomIndex].xsize < 1)
                        {
                            leftEdge = true;
                            diggerPositionX = rooms[roomIndex].xsize + 1;
                        }
                        // room below
                        if(diggerPositionY - rooms[roomIndex].ysize/2 < 1)
                        {
                            bottomEdge = true;
                            diggerPositionY = rooms[roomIndex].ysize/2 + 1;
                        }
                        // Get digger position to the local (0,0) of the room
                        diggerPositionX = diggerPositionX - rooms[roomIndex].xsize;
                        diggerPositionY = diggerPositionY - rooms[roomIndex].ysize/2;
                        break;

                };

                bool spotAvailable = true;
                // If it is a boss room, it can overwrite the other tiles
                if(!rooms[roomIndex].bossRoom)
                {
                    for(int j = 0; j < rooms[roomIndex].xsize; j++)
                    {
                        if(spotAvailable)
                        {
                            for(int k = 0; k < rooms[roomIndex].ysize; k++)
                            {
                                if(map[diggerPositionX +j, diggerPositionY +k] == floorTile)
                                {

                                    spotAvailable = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                // correctedDiggerX and Y refer to the bottom-leftmost point of the rooms
                // Rooms are build from this point
                int correctedDiggerX = diggerPositionX;
                int correctedDiggerY = diggerPositionY;
                if(spotAvailable)
                {
                    /*
                     * DIGS ROOM
                     * Uses the current roomIndex for the room
                     */
                   
                    for(int x = 0; x < rooms[roomIndex].xsize; x++)
                    {
                        diggerPositionX = correctedDiggerX;
                        diggerPositionX += x;

                        for(int y = 0; y < rooms[roomIndex].ysize; y++)
                        {
                            diggerPositionY = correctedDiggerY;
                            diggerPositionY += y;

                            //layFloor(diggerPositionX, diggerPositionY);
                            //print("dx "+ diggerPositionX + " dy "+ diggerPositionY + " y " + y + " ysize "+ rooms[roomIndex].ysize);
                            //map[diggerPositionX, diggerPositionY] = floorTile;
                            if(diggerPositionX >= mapDimensions.x - 1 || diggerPositionY >= mapDimensions.y -1)
                            {
                                Debug.Log("Direction: "+direction + " tempDigger: " + correctedDiggerX+", "+correctedDiggerY);
                            }
                            layFloor(diggerPositionX, diggerPositionY);
                        }

                    }
                    rooms[roomIndex].xpos = correctedDiggerX;
                    rooms[roomIndex].ypos = correctedDiggerY;
                }
                // Don't think I need these with the exit room algorithm below
                diggerPositionX = oldDiggerX;
                diggerPositionY = oldDiggerY;
                //roomIndex = 1;
                /*
                moveProb += probChange/2;
                turnProb += probChange/2;
                roomProb -= probChange;
                */
                // if spot was available, the room got built
                if(spotAvailable)
                {
                    /*
                     * EXIT ROOM
                     */

                    // This should make the edge bools in the room switch() obsolete
                    if(correctedDiggerY == mapDimensions.y - 1 - rooms[roomIndex].ysize)
                    {
                        topEdge = true;
                    }
                    if(correctedDiggerX == mapDimensions.x -1 - rooms[roomIndex].ysize)
                    {
                        rightEdge = true;
                    }
                    if(correctedDiggerY == 1)
                    {
                        bottomEdge = true;
                    }
                    if(correctedDiggerX == 1)
                    {
                        leftEdge = true;
                    }

                    // 0, 1, or 2
                    // 0 keeps going straight
                    // 1 turns right
                    // 2 would make you turn around. However, I don't want that
                    // so add 1 to 2 to make it 3
                    // I found when doing this for turning, subtracting and doing % gave the wrong answer
                    // Adding seemed like a better idea

                    int randDirection = Random.Range(0,3);
                    // Maybe turning around wouldn't be such a bad idea.... Maybe come back to that
                    if(randDirection == 2)
                    {
                        // special case. Add 1 more to turn left, not turn around
                        randDirection ++;
                    }
                    // extra checks to make sure you aren't going into a wall
                    if((topEdge &&(direction == 0)) || (rightEdge && (direction == 1)) || (bottomEdge && (direction == 2)) || (leftEdge && (direction == 3)))
                    {
                        if(((topEdge && leftEdge) && (direction == 0)) || ((topEdge && rightEdge) && (direction == 1)) || ((rightEdge && bottomEdge) && (direction == 2)) || ((bottomEdge && leftEdge) &&(direction == 3)))
                        {
                            // go right
                            randDirection = 1;
                        }
                        else
                        {
                            // can go left or right
                            randDirection = Random.Range(1, 2);
                        }
                    }
                    else if((topEdge &&(direction == 3)) || (rightEdge && (direction == 0)) || (bottomEdge && (direction == 1)) || (leftEdge && (direction == 2)))
                    {
                        if(((topEdge && leftEdge) && (direction == 2)) || ((topEdge && rightEdge) && (direction == 3)) || ((rightEdge && bottomEdge) && (direction == 0)) || ((bottomEdge && leftEdge) &&(direction == 1)))
                        {
                            // go left
                            randDirection = 3;
                        }
                        else
                        {
                            // straight or left
                            // 0 or 2
                            randDirection = Random.Range(0, 2);
                            // 0 or 1
                            // if 1, make it 2
                            if(randDirection == 1)
                            {
                                randDirection = 2;
                            }

                        }
                    }
                    else if((topEdge &&(direction == 1)) || (rightEdge && (direction == 2)) || (bottomEdge && (direction == 3)) || (leftEdge && (direction == 0)))
                    {
                        // straight or left
                        // 0 or 2
                        randDirection = Random.Range(0, 2);
                        // 0 or 1
                        // if 1, make it 2
                        if(randDirection == 1)
                        {
                            randDirection = 2;
                        }
                    }

                    direction = (direction + randDirection) % 4;
                    switch(direction)
                    {
                        case 0:
                            diggerPositionX = correctedDiggerX + rooms[roomIndex].xsize/2;
                            diggerPositionY = correctedDiggerY + rooms[roomIndex].ysize -1; // -1 to keep it inside the room
                            break;
                        case 1:
                            diggerPositionX = correctedDiggerX + rooms[roomIndex].xsize -1;
                            diggerPositionY = correctedDiggerY + rooms[roomIndex].ysize/2;
                            break;
                        case 2:
                            diggerPositionX = correctedDiggerX + rooms[roomIndex].xsize/2;
                            diggerPositionY = correctedDiggerY;
                            break;
                        case 3:
                            diggerPositionX = correctedDiggerX;
                            diggerPositionY = correctedDiggerY + rooms[roomIndex].ysize/2;
                            break;
                        
                    };

                    // Higher chance to move, lower chance to turn
                    // seeing as you want to dig one out to make a doorway
                    moveProb += roomProb;
                    //turnProb += roomProb;
                    roomProb = 0f;
                    roomIndex = (roomIndex + 1);
                }
                else
                {
                    // No spot was available here. Don't try to make a new room
                    moveProb += roomProb/2;
                    turnProb += roomProb/2;
                    roomProb = 0;
                }
            }
            //mapUpdate = false;

            loopCounter++;
        }
    }

    void createMap()
    {
        map = new Transform[(int)mapDimensions.x + 1, (int)mapDimensions.y + 1];
        for(int x = 0; x < mapDimensions.x; x++)
        {
            for(int y =0; y < mapDimensions.y; y++)
            {
                map[x, y] = wallTile;
            }
        }

    }

    public int getCurrentLevel()
    {
        return currentLevel;
    }

    [System.Serializable]
    public class Room{
        public int xsize;
        public int ysize;
        public int originalSizeX;
        public int originalSizeY;
        // stores where the room is in the array
        public int xpos;
        public int ypos;
        public Transform spawnType;
        public Transform originalSpawnType;
        public int numSpawns;
        public bool bossRoom;

    }
}
