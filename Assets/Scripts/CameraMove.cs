﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {

    public GameObject player;
    public float size = 6;

	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () 
	{
        transform.position = new Vector3(player.transform.position.x, 
                                         player.transform.position.y, -10);
        gameObject.GetComponent<Camera>().orthographicSize = size;
	}

	public void setPlayer()
	{
		player = GameObject.FindGameObjectWithTag("Player");
	}
}
