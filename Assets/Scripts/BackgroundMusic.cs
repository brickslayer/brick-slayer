﻿using UnityEngine;
using System.Collections;

public class BackgroundMusic : MonoBehaviour {

	public AudioClip mainSong;
	public AudioClip bossSong;
	public GameObject map;
	AudioSource source;
	int level;

	// Use this for initialization
	void Start () 
	{
		source = GetComponent<AudioSource>();
		source.clip = mainSong;
		source.Play();
		level = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{

		print(level);
		if (map.GetComponent<PGMapGenerator>().currentLevel > level)
		{
			level = map.GetComponent<PGMapGenerator>().currentLevel;
			if(map.GetComponent<PGMapGenerator>().currentLevel % 3 == 0)
			{
				print("here");
				source.Stop();
				source.clip = bossSong;
				source.Play();
			}
			else
			{
				if (!source.clip.Equals(mainSong))
				{
					source.Stop();
					source.clip = mainSong;
					source.Play();
				}

			}
		}
	}
}
