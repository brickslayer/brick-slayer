using UnityEngine;
using System.Collections;

public class EnemyMove : MonoBehaviour {
	
	//public GameObject player;     	// This requires you to drag-and-drop the player into the enemy
	public float EnemySpeed = 0.05f;	// Change this to change speed of enemy
	public int EnemyHealth = 10;
    public int EnemyMaxHealth;
	float deltaX;
	float deltaY;
	public PlayerController player;
	bool pinrange = false;
	double distance;
	
	public int EnemyAttackDamage = 10;
	public float EnemyAttackSpeed = 0.5f;
	private float nextFire = 0.0f;
	bool winrange;
	PlayerController target;
	public decision_tree tree;
	WallHP targetwall;
	public float temp;
	public float tempcol;
	public bool RND_Walk=false;
	public bool Walk=false;
    public bool movingToSpawn=false;
	public AstarAI Ai;
    public int moveDirection = 1;
	public float range;
    public int rangeThreshold = 3;
	//int rndwalk=5;
	//int rndwalksq= 25;


	// PowerUps
	public GameObject pickup1, pickup2, pickup3;

	
	// Use this for initialization
	void Start () {
        EnemyHealth = EnemyMaxHealth;
		player = FindObjectOfType(typeof(PlayerController)) as PlayerController;    // This allows the enemies to find the player instead. Should help when enemies are spawned randomly and not set in the scene
        tree = GetComponent<decision_tree>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		range = Vector2.Distance(player.transform.position , transform.position);
		if(range>=rangeThreshold)
			Ai.walk=true;
		if (RND_Walk) {
            //print("RND Walk. temp: "+temp);
			/*
            if(temp.Equals(tempcol)){
				rndwalk= -5;
				rndwalksq= -25;
				Invoke("RestoreWalks", 2f);
			}
			if (temp < .125)
				MoveTo (transform.position.x + rndwalk, transform.position.y + rndwalk);
			else if (temp < .25)
				MoveTo (transform.position.x + rndwalksq, transform.position.y);
			else if (temp < .375)
				MoveTo (transform.position.x + rndwalk, transform.position.y - rndwalk);
			else if (temp < .5)
				MoveTo (transform.position.x - rndwalk, transform.position.y + rndwalk);
			else if (temp < .625)
				MoveTo (transform.position.x - rndwalksq, transform.position.y);
			else if (temp < .75)
				MoveTo (transform.position.x - rndwalk, transform.position.y - rndwalk);
			else if (temp < .875)
				MoveTo (transform.position.x, transform.position.y + rndwalksq);
			else
				MoveTo (transform.position.x, transform.position.y - rndwalksq);
    */
            transform.position = Vector2.MoveTowards(transform.position, tree.randomWalkTarget, EnemySpeed*moveDirection);
		} 
		else if (Walk) {
            //print("Walk");
			//Move();
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, EnemySpeed * Time.deltaTime);
		}
        else if(movingToSpawn)
        {
            transform.position = Vector2.MoveTowards(transform.position, tree.Spawn, EnemySpeed);
        }

		if (EnemyHealth == 0) {
			DropItems();
            if(GetComponent<Judge>())
            {
                GetComponent<Judge>().createExit();
            }
			Destroy(gameObject);
		}
		if(pinrange) {
			if (Time.time >= nextFire) {
				
				if (target != null) {
					target.takeDamage (EnemyAttackDamage);
					nextFire = Time.time + EnemyAttackSpeed;
				}
			}
		}
		if (winrange) {
			if (Time.time >= nextFire) {
				if (targetwall != null) {
					targetwall.takeDamage (EnemyAttackDamage);
					nextFire = Time.time + EnemyAttackSpeed;
				}
			}
		}
	}
	public void RestoreWalks(){
        //print("RestoreWalks running");
		//rndwalk= 5;
		//rndwalksq= 25;
		//tree.NotOnWall = true;
	}
    /*
	public void Move()
	{
		deltaX = player.transform.position.x - GetComponent<Rigidbody2D> ().position.x;
		deltaY = player.transform.position.y - GetComponent<Rigidbody2D> ().position.y;
		// Values so enemy doesn't run into you and push you around
		
		if (System.Math.Abs (deltaX) > 0.6f || System.Math.Abs (deltaY) > 0.7f) {
			// Used trig to keep enemy moving at a uniform speed always
			// Otherwise, goes faster when further away and slower close to you
			float h = (float)System.Math.Sqrt (deltaY * deltaY + deltaX * deltaX);
			Vector3 toPlayer = new Vector3 (deltaX / h * EnemySpeed, deltaY / h * EnemySpeed);
			GetComponent<Rigidbody2D> ().transform.Translate (toPlayer);
		}
	}
	
	public void MoveTo (float x, float y){
		if (System.Math.Abs (x) > 0.6f || System.Math.Abs (y) > 0.7f) {
        //Debug.Log("MoveTo: "+x+", "+y);
			float h = (float)System.Math.Sqrt (y * y + x * x);
			Vector3 tolocation = new Vector3 (x / h *  EnemySpeed, y / h * EnemySpeed );
			GetComponent<Rigidbody2D> ().transform.Translate (tolocation);
		}
	}
 */   
	
	public void takeDamage(int dmg){
		if (dmg > EnemyHealth) {
			EnemyHealth = 0;
		} else {
			EnemyHealth -= dmg;
		}
	}
	
	public int getHealth()
	{
		return EnemyHealth;
	}

    public int getMaxHealth()
    {
        return EnemyMaxHealth;
    }

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") {
			pinrange = true;
			target = other.gameObject.GetComponent<Collider2D> ().GetComponent<PlayerController> ();
		} else if (other.gameObject.tag == "BrickWall") {
			winrange = true;
			targetwall = other.gameObject.GetComponent<Collider2D> ().GetComponent<WallHP> ();
		} else if (other.gameObject.tag == "Wall") {
			tempcol=temp;

            moveDirection *=-1;
			tree.NotOnWall=false;
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if (other.gameObject.tag == "Player") {
			pinrange = false;
		} else if (other.gameObject.tag == "Wall") {
			winrange = false;


            tree.NotOnWall = true;
		}
	}

	public void DropItems()
	{
		int item = Random.Range(1, 100);
		float healthProb = 0.25f;	// % chance to drop
        Debug.Log("item: "+item + "prob: "+ (100 - (player.playerMaxHealth - player.playerHealth)*healthProb));
		if(item > (100 - (player.playerMaxHealth - player.playerHealth)*healthProb) && item < 101)
		{

			// The lower health you get, the higher the probability of getting a health pickup
			// Up to a max of 25% as you get to 0% health
			// The probability is 0 when you are max health
			Instantiate(pickup1, this.transform.position, this.transform.rotation);
		}
		else if(item > 50 && item < 53)
		{
			// Drop item 2
			Instantiate(pickup2, this.transform.position, this.transform.rotation);
		}
		else if(item >0 && item <6)
		{
			
			Instantiate(pickup3, this.transform.position, this.transform.rotation);
		}
	}
}

