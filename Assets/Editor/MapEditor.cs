﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof (PGMapGenerator))]
public class MapEditor : Editor {
    
    public override void OnInspectorGUI ()
    {
        
        PGMapGenerator map = target as PGMapGenerator;


        if(DrawDefaultInspector())
        {
            map.DestroyMapImmediate();
            map.GenerateMap ();
        }


        if(GUILayout.Button("Get New Map"))
        {
            map.DestroyMapImmediate();
            map.GenerateMap();
        }
    }
    
}